<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Fazle
 * Date: 27/07/13
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */

function getPageSections() {
    $args=array(
        'capability_type'   => 'page'
    );
    $output = 'names'; // names or objects, note names is the default
    return get_post_types($args,$output);
}

function mbox_plugin_activate() {
    global $wpdb;
    include dirname(__FILE__)."/install.php";
    mbox_install();
}

function mbox_plugin_admin_init() {
    /* Register our script. */
    wp_register_style( 'mbox-plugin-style', plugins_url('/assets/jquery.fancybox.css', __FILE__) );
    wp_register_script( 'mbox-plugin-script', plugins_url( '/assets/jquery.fancybox.js', __FILE__ ) );
}
function mbox_plugin_admin_scripts() {
    /* Link our already registered script to a page */
    wp_enqueue_style( 'mbox-plugin-style' );
    wp_enqueue_script( 'mbox-plugin-script' );
}

function getPagesBySections($sections_name){
    global $wpdb;
    $sql = "SELECT * FROM {$wpdb->prefix}posts where post_type='{$sections_name}'";
    return $wpdb->get_results($sql);
}

function mbox_plugin_menu() {
    add_menu_page('mBox', 'mBox', 'manage_options', 'mbox-administration', 'mbox_admin_menu');
}

function mbox_admin_menu() {
    if (!current_user_can('manage_options'))  {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    echo '<div class="wrap">';
    include_once "list.php";
    echo '</div>';
}

function mbox_add_menu() {
    $suffix = add_submenu_page('mbox-administration', 'Add New', 'Add New', 'manage_options','mbox-add-new' ,'mobx_add_new');
    add_action('admin_print_scripts-' . $suffix, 'mbox_plugin_admin_scripts');
}

function mobx_add_new(){
    echo '<div class="wrap">';
    if(isset($_GET['id'])){
        $mbox = new mBox($_GET['id']);
    } else {
        $mbox = new mBox();
    }

    include_once dirname(__FILE__)."/addnew.php";
    echo '</div>';
}

function save_mbox(){
    if(isset($_POST['mbox_albelli'])) {
        $mbox = null;
        if(isset($_POST['id'])){
            $mbox = new mBox($_POST['id']);
        } else {
            $mbox = new mBox();
        }

        if($mbox instanceof mBox){

            $mbox->hydrad($_POST);
            $mbox->save();
            $rd = site_url().'/wp-admin/admin.php?page=mbox-add-new&id='.$mbox->getId();
            wp_redirect($rd);
        }
    }
}

function delete_mbox()
{

}
