<?php

/****************************************************************/
/* Install routine for mbox plugin
/****************************************************************/

function mbox_install()
{
    global $wpdb;
    // set tablename
    $table_name = $wpdb->prefix . 'mbox';

    $mbox_found = false;

    foreach ($wpdb->get_results("SHOW TABLES;", ARRAY_N) as $row) {

        if ($row[0] == $table_name) $mbox_found = true;

    }

    if (!$mbox_found) {
        $sql = "CREATE TABLE `{$table_name}` (
        `id` INT(10) NOT NULL AUTO_INCREMENT,
        `description` TEXT NULL DEFAULT NULL,
        `id_to_wrap` VARCHAR(250) NULL DEFAULT NULL,
        `id_to_wrap_active` TINYINT NULL DEFAULT NULL,
        `class_to_wrap` VARCHAR(250) NULL DEFAULT NULL,
        `class_to_active` TINYINT NULL DEFAULT NULL,
        `pre_js` TEXT NULL DEFAULT NULL,
        `post_js` TEXT NULL DEFAULT NULL,
        `insert_on_pages` TEXT NULL DEFAULT NULL,
        `exclude_on_page` TEXT NULL DEFAULT NULL,
        `status` VARCHAR(250) NULL DEFAULT NULL,
        `author_id` TINYINT NULL DEFAULT NULL,
        `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )
    COLLATE='utf32_general_ci'
    ENGINE=InnoDB;";
        $wpdb->query($sql);
    }

}