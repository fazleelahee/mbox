<div id="icon-plugins" class="icon32">
</div>
<?php if($mbox instanceof mBox): ?>
<h2><?php $mbox->getId() =="" ?_e("Add New"):_e("Edit")?></h2>

<form method="POST">
    <input type="hidden" name="mbox_albelli" value="mbox">
    <input type="hidden" name="id" value="<?=$mbox->getId(); ?>">
    <?php
    if($mbox->getId() == ''){
        $current_user = wp_get_current_user();
        echo '<input type="hidden" name="id" value="'.$current_user->ID.'">';
    }
    ?>

    <table class="wp-list-table widefat fixed posts">
        <tr>
            <th>Description</th>
            <td><textarea name="description" cols="100" rows="2"><?=$mbox->getDescription(); ?></textarea></td>
        </tr>
        <tr>
            <th>Status</th>
            <td>
                <select name="status">
                    <option value="1" <?php echo $mbox->getStatus() == 1? 'selected': '';?>>Enabled</option>
                    <option value="0" <?php echo $mbox->getStatus() == 0? 'selected': '';?>>Disabled</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>Which ID to Wrap?</th>
            <td>
                <?php $id_to_wrap = array('Header', 'Wrap-content','Wrap-sidebar', 'Wrap-footer', 'wrap-sitemap'); ?>
                <select name="id_to_wrap">
                    <?php
                        foreach($id_to_wrap as $value)
                        {
                            $selected = $mbox->getIdToWrap() == $value? 'selected': '';
                            echo "<option value='{$value}' $selected >{$value}</option>";
                        }
                    ?>
                </select>
                <input type="checkbox" value="1" name="id_to_wrap_active" <?php echo $mbox->getIdToWrapActive()==1? 'checked': ''; ?> > Activate
            </td>
        </tr>
        <tr>
            <th>Which Class to Wrap</th>
            <td>

                <?php $class_to_wrap = array('Product Title', 'Product price','Layer', 'Download box', 'Online Box'); ?>
                <select name="class_to_wrap">
                    <?php
                    foreach($class_to_wrap as $value)
                    {
                        $selected = $mbox->getClassToWrap() == $value? 'selected': '';
                        echo "<option value='{$value}'  $selected >{$value}</option>";
                    }
                    ?>
                </select>
                <input type="checkbox" value="1" name="class_to_active" <?php echo $mbox->getClassToActive()==1? 'checked': ''; ?> > Activate

            </td>
        </tr>
        <tr>
            <th>Pre JS</th>
            <td><textarea name="pre_js" cols="100" rows="6"><?=$mbox->getPreJs()?></textarea></td>
        </tr>
        <tr>
            <th>Post JS</th>
            <td><textarea name="post_js" cols="100" rows="6"><?=$mbox->getPostJs()?></textarea></td>
        </tr>
        <tr>
            <th>Insert On Pages</th>
            <td><a class="popup-box"  href="#insert_on_page">[...]</a></td>
        </tr>
        <tr>
            <th>Exclude pages</th>
            <td><a class="popup-box" href="#exclude_page">[...]</a></td>
        </tr>
        <tr>
            <th><input class='button-primary' type='submit' name='save' value='Save Your mBox' id='submitbutton' /></th>
            <td></td>
        </tr>
    </table>
    <?php $post_types = getPageSections();?>

    <div id="insert_on_page" style="display: none">
        <table>
            <tr><th>page slug</th><th>Include?</th></tr>
            <tr>
                <td><h3>Section</h3></td><td></td>
            </tr>
            <?php
            foreach ($post_types as $post_type ) {
                echo '<tr><td>'. $post_type. '</td><td>';
                echo '<input type="checkbox" value="'.$post_type.'" name="inc_all_'.$post_type.'" ></td></tr>';
            }
            ?>

            <?php
            foreach ($post_types as $post_type ) {
                echo "<tr>
            <td><h3>{$post_type}</h3></td><td></td>
            </tr>";

                $results = getPagesBySections($post_type);
                if(!empty($results)){
                    foreach($results as $r){
                        echo '<tr><td>/'. $r->post_name. '</td><td>';
                        echo '<input type="checkbox" value="'.$r->ID.'" name="inc_all_'.$r->post_name.'_ids" ></td></tr>';
                    }
                }

            }?>

        </table>

    </div>

    <div id="exclude_page" style="display: none">
       <table>
        <tr><th>page slug</th><th>Excluded?</th></tr>
        <tr>
            <td><h3>Section</h3></td><td></td>
        </tr>
        <?php
        foreach ($post_types as $post_type ) {
            echo '<tr><td>'. $post_type. '</td><td>';
            echo '<input type="checkbox" value="'.$post_type.'" name="ex_all_'.$post_type.'" ></td></tr>';
        }
        ?>

        <?php
        foreach ($post_types as $post_type ) {
            echo "<tr>
            <td><h3>{$post_type}</h3></td><td></td>
            </tr>";

            $results = getPagesBySections($post_type);
            if(!empty($results)){
                foreach($results as $r){
                    echo '<tr><td>/'. $r->post_name. '</td><td>';
                    echo '<input type="checkbox" value="'.$r->ID.'" name="ex_all_'.$r->post_name.'_ids" ></td></tr>';
                }
            }

        }?>

        </table>
    </div>
</form>



<script>
    jQuery(document).ready(function($) {
        $(".popup-box").fancybox({
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    });
</script>
<?php endif;?>