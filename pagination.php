<?php
class Pagination
{

    public $start = 0;
    public $limit = 5;
    public $pagination = "";
    private $targetpage = "";

    // constructor
    public function __construct($target, $per_page)
    {
        $this->limit = $per_page;
        $this->targetpage = $target;
    }

    public function make_pagination($total_row)
    {
        $targetpage = $this->targetpage;
        $limit = $this->limit;
//$query="SELECT COUNT(*) as num FROM $tableName";
//$num = $wpdb->get_row($query);
        $total_pages = $total_row;

        $stages = 3;
        $page = isset($_GET['page_num']) ? mysql_escape_string($_GET['page_num']) : "1";
        if ($page) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

// Initial page num setup
        if ($page == 0) {
            $page = 1;
        }
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages / $limit);
        $LastPagem1 = $lastpage - 1;

        $this->start = $start;

        $paginate = '';
        if ($lastpage > 1) {

            $paginate .= "<div class='paginate'>";
            // Previous
            if ($page > 1) {
                $paginate .= "<a href='$targetpage&page_num=$prev'>&nbsp;<<&nbsp;</a>";
            } else {
                $paginate .= "<span class='disabled'>&nbsp;<<&nbsp;</span>";
            }


            // Pages
            if ($lastpage < 7 + ($stages * 2)) // Not enough pages to breaking it up
            {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page) {
                        $paginate .= "<span class='current'>$counter</span>";
                    } else {
                        $paginate .= "<a href='$targetpage&page_num=$counter'>$counter</a>";
                    }
                }
            } elseif ($lastpage > 5 + ($stages * 2)) // Enough pages to hide a few?
            {
                // Beginning only hide later pages
                if ($page < 1 + ($stages * 2)) {
                    for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                        if ($counter == $page) {
                            $paginate .= "<span class='current'>$counter</span>";
                        } else {
                            $paginate .= "<a href='$targetpage&page_num=$counter'>$counter</a>";
                        }
                    }
                    $paginate .= "...";
                    $paginate .= "<a href='$targetpage&page_num=$LastPagem1'>$LastPagem1</a>";
                    $paginate .= "<a href='$targetpage&page_num=$lastpage'>$lastpage</a>";
                } // Middle hide some front and some back
                elseif ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                    $paginate .= "<a href='$targetpage&page_num=1'>1</a>";
                    $paginate .= "<a href='$targetpage&page_num=2'>2</a>";
                    $paginate .= "...";
                    for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                        if ($counter == $page) {
                            $paginate .= "<span class='current'>$counter</span>";
                        } else {
                            $paginate .= "<a href='$targetpage&page_num=$counter'>$counter</a>";
                        }
                    }
                    $paginate .= "...";
                    $paginate .= "<a href='$targetpage&page_num=$LastPagem1'>$LastPagem1</a>";
                    $paginate .= "<a href='$targetpage&page_num=$lastpage'>$lastpage</a>";
                } // End only hide early pages
                else {
                    $paginate .= "<a href='$targetpage&page_num=1'>1</a>";
                    $paginate .= "<a href='$targetpage&page_num=2'>2</a>";
                    $paginate .= "...";
                    for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page) {
                            $paginate .= "<span class='current'>$counter</span>";
                        } else {
                            $paginate .= "<a href='$targetpage&page_num=$counter'>$counter</a>";
                        }
                    }
                }
            }

            // Next
            if ($page < $counter - 1) {
                $paginate .= "<a href='$targetpage&page_num=$next'>&nbsp;>>&nbsp;</a>";
            } else {
                $paginate .= "<span class='disabled'>&nbsp;>>&nbsp;</span>";
            }

            $paginate .= "</div>";
        }
        $this->pagination = $paginate;
    }
}