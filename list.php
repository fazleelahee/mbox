<?php
global $wpdb;
include_once "pagination.php";
?>
<style>
    .paginate {
        padding: 3px;
        margin: 3px;
    }

    .paginate a {
        padding:2px 5px 2px 5px;
        margin:2px;
        border:1px solid #999;
        text-decoration:none;
        color: #666;
    }
    .paginate a:hover, .paginate a:active {
        border: 1px solid #999;
        color: #000;
    }
    .paginate span.current {
        margin: 2px;
        padding: 2px 5px 2px 5px;
        border: 1px solid #999;

        font-weight: bold;
        background-color: #999;
        color: #FFF;
    }
    .paginate span.disabled {
        padding:2px 5px 2px 5px;
        margin:2px;
        border:1px solid #eee;
        color:#DDD;
    }
</style>
<div id="icon-plugins" class="icon32">
</div> <h2>mBox</h2>
<br />
<?php

if(isset($_POST['apply']) && isset($_POST['bulk_actions']) && $_POST['bulk_actions'] == "delete")
{
    //call delete
}
else
{
    //do nothing.
}

$sql = "SELECT COUNT(*) AS num FROM {$wpdb->prefix}mbox";
$row = $wpdb->get_row($sql);
$link = explode("?",$_SERVER['REQUEST_URI']);

$ob = new Pagination($link[0]."?page=".$_GET['page'],20);
$ob->make_pagination($row->num);

$sql = "SELECT * FROM {$wpdb->prefix}mbox ORDER BY description LIMIT ".$ob->start.",".$ob->limit;
$mbox = $wpdb->get_results($sql);
if(!empty($mbox))
{

    ?>
    <div style="text-align:right">
        <?php  echo $ob->pagination; ?>
    </div>
    <form method="post" action="">
        <table name="contributor-list" class="wp-list-table widefat fixed posts" cellspacing="0">
            <tr>
                <th class='manage-column' style="width:30px"><input name="select-all" class="select-all" type="checkbox" value="" /></th>
                <th class='manage-column'><strong>Description</strong></th>
                <th class='manage-column'><strong>Location</strong></th>
                <th class='manage-column'><strong>Sections</strong></th>
                <th class='manage-column'><strong>Status</strong></th>
            </tr>
            <?php
            foreach( $mbox as $c )
            {

                $link = "<a href='".site_url()."/wp-admin/admin.php?page=mbox-add-new&id=".$c->id."'>".$c->description. "</a>";
                $status = $c->status == "1"? "Online" : "Offline";
                ?>
                <tr>
                    <td class='manage-column' style="text-align:center"><input name="mbox[]" type="checkbox" value="<?php _e($c->id);?>" class="con_id" /></td>
                    <td><?php echo  $link; ?></td>
                    <td></td>
                    <td></td>
                    <td><?php _e($status);?></td>
                </tr>
            <?php
            }

            ?>
        </table>
        <div style="padding-top:10px">
            <select name="bulk_actions">
                <option value="">Bulk Actions</option>
                <option value="delete">Delete</option>
            </select>
            &nbsp;&nbsp;&nbsp;

            <input name="apply" value="Apply" type="submit" class="button-secondary" />
        </div>

    </form>
<?php


}
?>

<script>
    jQuery(document).ready(function ($) {
        $(".select-all").click(function() {
            if($(this).attr("checked"))
            {
                $(".con_id").attr("checked",true);
            }
            else
            {
                $(".con_id").attr("checked",false);
            }
        });
    });
</script>