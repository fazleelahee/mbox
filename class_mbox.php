<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Fazle
 * Date: 27/07/13
 * Time: 09:30
 * To change this template use File | Settings | File Templates.
 */

class mBox{
    private $id;
    private $description;
    private $id_to_wrap;
    private $id_to_wrap_active;
    private $class_to_active;


    private $class_to_wrap;
    private $pre_js;
    private $post_js;
    private $insert_on_pages;
    private $exclude_on_page;
    private $status;
    private $author_id;
    private $created_at;

    public function setClassToActive($class_to_active)
    {
        $this->class_to_active = $class_to_active;
    }

    public function getClassToActive()
    {
        return $this->class_to_active;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setAuthorId($author_id)
    {
        $this->author_id = $author_id;
    }

    public function getAuthorId()
    {
        return $this->author_id;
    }

    public function setClassToWrap($class_to_wrap)
    {
        $this->class_to_wrap = $class_to_wrap;
    }

    public function getClassToWrap()
    {
        return $this->class_to_wrap;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setExcludeOnPage($exclude_on_page)
    {
        $this->exclude_on_page = $exclude_on_page;
    }

    public function getExcludeOnPage()
    {
        return $this->exclude_on_page;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setIdToWrap($id_to_wrap)
    {
        $this->id_to_wrap = $id_to_wrap;
    }

    public function getIdToWrap()
    {
        return $this->id_to_wrap;
    }

    public function setInsertOnPages($insert_on_pages)
    {
        $this->insert_on_pages = $insert_on_pages;
    }

    public function getInsertOnPages()
    {
        return $this->insert_on_pages;
    }

    public function setPostJs($post_js)
    {
        $this->post_js = $post_js;
    }

    public function getPostJs()
    {
        return $this->post_js;
    }

    public function setPreJs($pre_js)
    {
        $this->pre_js = $pre_js;
    }

    public function getPreJs()
    {
        return $this->pre_js;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }
    public function setClassToActiveActive($class_to_active_active)
    {
        $this->class_to_active_active = $class_to_active_active;
    }

    public function getClassToActiveActive()
    {
        return $this->class_to_active_active;
    }

    public function setIdToWrapActive($id_to_wrap_active)
    {
        $this->id_to_wrap_active = $id_to_wrap_active;
    }

    public function getIdToWrapActive()
    {
        return $this->id_to_wrap_active;
    }
    public function __construct($id = '') {
        if(!empty($id)){
            global $wpdb;
            $sql = "SELECT * FROM {$wpdb->prefix}mbox WHERE id='{$id}'";
            $row = $wpdb->get_row($sql,ARRAY_A);

            if(!empty($row)){
                  $this->hydrad($row);
            }
        }
    }

    public function hydrad($data) {
        foreach($data as $key=>$val){
            if(property_exists(new mBox(), $key)){
                $this->$key = $val;
            }
        }
    }
    public function save() {
        global $wpdb;
        if($this->id == ''){
            $data = get_object_vars ( $this );
            unset($data['id']);
            $keys = array();
            $vals = array();
            foreach($data as $key => $val){
                $keys[] = $key;
                $vals[] = "'".mysql_real_escape_string($val)."'";
            }

            $sql = "INSERT INTO {$wpdb->prefix}mbox (".implode(',',$keys).") VALUES(".implode(',',$vals).")";
            $wpdb->query($sql);
            $this->id = $wpdb->insert_id;

        } else {
            $data = get_object_vars ( $this );
            unset($data['id']);
            $wpdb->update( $wpdb->prefix."mbox", $data, array('id'=>$this->id));
        }
    }

    public function delete() {
        global $wpdb;
        $sql = 'DELETE FROM '.$wpdb->prefix.'mbox WHERE id="'.$this->id.'"';
        $wpdb->query($sql);
    }


}