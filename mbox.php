<?php
/*
Plugin Name: Mbox
Plugin URI: #
Description: practical assignment for interview process!
Author: Fazle Elahee
Version: 1.0
Author URI: #
*/
include_once "class_mbox.php";
include_once "functions.php";

register_activation_hook( __FILE__, 'mbox_plugin_activate' );

add_action( 'admin_init', 'mbox_plugin_admin_init' );
add_action('admin_menu', 'mbox_plugin_menu');
add_action('admin_menu', 'mbox_add_menu');
add_action('init', 'save_mbox');

